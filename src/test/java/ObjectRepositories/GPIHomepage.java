package ObjectRepositories;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GPIHomepage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public GPIHomepage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	public void GetTitle() throws InterruptedException {

		Thread.sleep(8000);
		ArrayList<Object> tabs2 = new ArrayList<Object>(driver.getWindowHandles());
		// System.out.println(tabs2.size());
		driver.switchTo().window((String) tabs2.get(1));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".home_anchor")));
		String title = driver.getTitle();
		// System.out.println(driver.getTitle());
		if (title.equals("gpi Tracker")) {
			System.out.println("Login to GPI tracker is successful");
			System.out.println("Title of the Page : " + title);
			String uName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.ng-binding")))
					.getText();
			System.out.println("Username : " + uName);
		} else {
			System.out.println("Login to GPI tracker is failed");
			System.out.println("Title of the Page : " + title);
		}
		this.GPILogout();
	}

	public void GPILogout() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("span.ng-binding"))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li.user_dropdown__link:nth-child(9)")))
				.click();
		Thread.sleep(3000);
		driver.close();
	}

}
