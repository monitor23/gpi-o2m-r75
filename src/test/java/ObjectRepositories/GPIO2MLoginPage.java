package ObjectRepositories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GPIO2MLoginPage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public GPIO2MLoginPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(css = "div.auth_HL:nth-child(2")
	WebElement loginoption;
	@FindBy(id = "gwt-debug-platform_login-username")
	WebElement username;
	@FindBy(id = "gwt-debug-platform_login-password")
	WebElement password;
	@FindBy(id = "gwt-debug-platform_login-logon")
	WebElement login;

	public WebElement LoginOption() {
		wait.until(ExpectedConditions.visibilityOf(loginoption));
		return loginoption;
	}

	public void WaitFunction() {

		wait.until(ExpectedConditions.visibilityOf(username));

	}

	public WebElement Username() {

		return username;
	}

	public WebElement Password() {
		return password;
	}

	public WebElement Login() {
		return login;
	}

}
