package ObjectRepositories;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GPIO2MHomePage {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public GPIO2MHomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 300);
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "gwt-debug-desktop-mainmenu-com.swift.WebAccessIdp.o2m001")
	WebElement o2mLink;
	@FindBy(id = "gwt-debug-desktop-mainmenu-com.swift.WebAccessIdp.gpi001")
	WebElement gpiLink;

	public WebElement O2MLink() {
		wait.until(ExpectedConditions.visibilityOf(o2mLink));
		return o2mLink;
	}

	public WebElement GPILink() {
		wait.until(ExpectedConditions.visibilityOf(gpiLink));
		return gpiLink;
	}

	public void GPIO2MLogout() throws InterruptedException {
		Thread.sleep(2000);
		ArrayList<Object> tabs2 = new ArrayList<Object>(driver.getWindowHandles());
		// System.out.println(tabs2.size());
		driver.switchTo().window((String) tabs2.get(0));
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-desktop-topmenu-Logout"))).click();
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-debug-dialog-ask-0-ok"))).click();
		Thread.sleep(2000);
		driver.close();
	}

}
