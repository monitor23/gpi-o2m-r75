package TestCases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.GPIO2MHomePage;
import ObjectRepositories.GPIO2MLoginPage;
import ObjectRepositories.O2MHomepage;

public class O2MDashboard {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://swp75.netlink-testlabs.com:2443/swp/customgroup/itb75/");

	}

	@Test
	public void O2MVerification() throws InterruptedException {
		GPIO2MLoginPage golp = new GPIO2MLoginPage(driver);
		golp.LoginOption().click();
		golp.WaitFunction();
		golp.Username().sendKeys("userathena");
		golp.Password().sendKeys("Venus2009Venus2009+");
		golp.Login().click();
		GPIO2MHomePage gohp = new GPIO2MHomePage(driver);
		gohp.O2MLink().click();
		O2MHomepage ohp = new O2MHomepage(driver);
		ohp.GetTitle();
		gohp.GPIO2MLogout();

	}

}
